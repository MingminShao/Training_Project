package com.example.citi_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CitiProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(CitiProjectApplication.class, args);
    }

}
